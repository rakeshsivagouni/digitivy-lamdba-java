package com.digitivy.templates;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class HelloHandlerTest {

    @Test
    public void not_null_result() {
        Context context = mock(Context.class);
        doReturn(mock(LambdaLogger.class)).when(context).getLogger();

        //when
        String returnedString = handler.handleRequest("John Doe", context);

        //then
        Assert.assertNotNull(returnedString);
        Assert.assertEquals("Hello, John Doe!" , returnedString);
    }

    private HelloHandler handler = new HelloHandler();

}

