# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2019-04-17
### Added
 - Save Artifact to Downloads directory step

## [1.0.1] - 2019-04-17
### Added
 - Jacoco Code Coverage Verification

## [1.0.0] - 2019-04-15
### Added
 - Initial project versioning